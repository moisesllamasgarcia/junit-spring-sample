package com.tanuki.testingweb;

import org.springframework.stereotype.Service;

@Service
public class GreetingService {
    public String greet() {
        return "Greetings from GitLab!";
    }
}